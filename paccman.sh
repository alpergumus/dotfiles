#!/bin/bash

sudo pacman -S --no-confirm xorg xorg-xinit xorg-server xorg-xrandr xorg-xsetroot nvidia nvidia-utils xf86-video-intel intel-ucode xdg-utils xdg-user-dirs pulseaudio pulseaudio-bluetooth pulseaudio-alsa pulseaudio-equalizer pulseaudio-jack base-devel linux-headers bluez bluez-utils network-manager-applet wireless_tools wpa_supplicant picom alacritty kitty neofetch flatpak curl wget pcmanfm unrar udisks2 tar neofetch alsa-utils vlc firefox engrampa vim emacs zathura zathura-pdf-mupdf lxappearance nitrogen atom libreoffice qbittorrent xmonad xmonad-contrib xmobar xterm dmenu 
