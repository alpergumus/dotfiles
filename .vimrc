syntax enable
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set guicursor=
set relativenumber
set nu
set nohlsearch
set hidden
set noerrorbells
set expandtab
set nowrap
set noswapfile
set nobackup
set incsearch
set termguicolors
set scrolloff=8
set signcolumn=yes
set updatetime=50

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'jnurmine/zenburn'
call plug#end()

set background=dark
colorscheme zenburn

let &t_ut=''
